import java.io.*;
import java.net.*;
/*
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
*/

public class RiderStatusClient {
    public static void main( String[] args ) {
	try {
		// Authentication token (password)
		String auth_token = new String("8241b6ba744ddb5652266ce93249482fa864624c");

		// URL to send request to. https and Trailing slash are needed!
		URL url = new URL("https://ramrod.cdine.org/api/riderstatus/");

		HttpURLConnection con = (HttpURLConnection) url.openConnection();

		con.setDoOutput(true);
		con.setDoInput(true);
		con.setRequestMethod("POST");
		con.setRequestProperty("Content-Type", "application/vnd.api+json");
		con.setRequestProperty("Accept", "application/vnd.api+json");
		con.setRequestProperty("Authorization", "Token" + " " + auth_token);


		/*
		// Request body - JSON Object (Needs dependencies.. good to go this route, but I don't have maven or anything setup
		JsonObject value = Json.createObjectBuilder()
			.add("data", Json.createObjectBuilder()
					.add("type", "RiderStatus")
			.add("attributes", Json.createObjectBuilder()
					.add("bib_number", 9999)
					.add("cpcode", "STX")
					.add("status", "RFD")
					.add("crossing_time", "10:15")))
			.build();
		*/
		
		// Request body - JSON String
		StringBuilder sb = new StringBuilder();
		sb.append("{");
		sb.append("\"data\":");
		sb.append("{");
		sb.append("\"type\": \"RiderStatus\",");
		sb.append("\"attributes\":");
		sb.append("{");
		sb.append("\"bib_number\": 9999,");
		sb.append("\"cpcode\": \"STX\",");
		sb.append("\"status\": \"RFD\",");
		sb.append("\"crossing_time\": \"10:15\"");
		sb.append("}");
		sb.append("}");
		sb.append("}");

		String json_request_body = sb.toString();

		// DEBUG - Print the request body
		System.out.println("JSON Request Body:");
		System.out.println("==================");
		System.out.println(json_request_body);
		System.out.println("==================\n\n");

		// Timeout - allow 5 seconds to write and read
		con.setConnectTimeout(5000);
		con.setReadTimeout(5000);

		// Send the request
		OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
		wr.write(json_request_body);
		wr.flush();

		// Response code - should be 201 for "created"
		int status = con.getResponseCode();

		// Ensure status is 201
		assert HttpURLConnection.HTTP_CREATED == status : "HTTP Status Code was NOT 201 Created";

		// Finally, let’s read the response of the request and place it in a content String
		BufferedReader in = new BufferedReader(
		new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer content = new StringBuffer();
		while ((inputLine = in.readLine()) != null) {
		    content.append(inputLine);
		}
		in.close();

		// Close the underlying connection
		con.disconnect();

		// DEBUG: Print the response
		System.out.println("HTTP Response Body:");
		System.out.println("===================");
		System.out.println(content);
		System.out.println("===================");
	}
	catch (Exception e) {
		    System.err.println("Caught some Exception: " + e.getMessage());
	}

        System.exit( 0 ); //success
    }
}
