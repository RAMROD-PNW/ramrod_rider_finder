RAMROD Rider Finder
===================

Web App To Look Up Rider Checkpoint Crossing Times For The RAMROD [Ride Around Mount Rainier in One Day] Bicycle Ride

.. image:: https://img.shields.io/badge/built%20with-Cookiecutter%20Django-ff69b4.svg
     :target: https://github.com/pydanny/cookiecutter-django/
     :alt: Built with Cookiecutter Django


:License: MIT

Example JSON-API HTTP POST Request Message (Single):
----------------------------------------------------

POST `/api/riderstatus/`

.. code-block:: json

 {
   "data": {
     "type": "RiderStatus",
     "attributes": {
       "bib_number": 23,
       "cpcode": "STX",
       "status": "RFD",
       "crossing_time": "18:37"
     }
   }
 }


Example JSON-API HTTP POST Request Message (Bulk):
--------------------------------------------------

Do not include any CSV header. Separate CSV rows by newlines or carriage-return/newlines.

POST `/api/riderstatusbulk/`

.. code-block:: json

 {
   "data": {
     "type": "RiderStatusBulk",
      "attributes": {
        "csv_text": "1,STX,RFD,05:00\n2,STX,RFD,05:01\n6,STX,MAN,05:03"
     }
   }
 }


Example Response:

.. code-block:: http

 HTTP 201 Created
 Allow: GET, POST, HEAD, OPTIONS
 Content-Type: application/vnd.api+json
 Location: http://localhost:8000/api/riderstatusbulk/11/
 Vary: Accept
 
 {
     "data": {
         "type": "RiderStatusBulk",
         "id": "11",
         "attributes": {
             "csv_text": "1,STX,RFD,05:00\n2,STX,RFD,05:01\n6,STX,MAN,05:03",
             "csv_received_time": "2019-07-18T22:28:37.135805-07:00",
             "csv_data_commited": true
         },
         "links": {
             "self": "http://localhost:8000/api/riderstatusbulk/11/"
         }
     }
 }

Settings
--------

Moved to settings_.

.. _settings: http://cookiecutter-django.readthedocs.io/en/latest/settings.html

Basic Commands
--------------

Setting Up Your Users
^^^^^^^^^^^^^^^^^^^^^

* To create a **normal user account**, just go to Sign Up and fill out the form. Once you submit it, you'll see a "Verify Your E-mail Address" page. Go to your console to see a simulated email verification message. Copy the link into your browser. Now the user's email should be verified and ready to go.

* To create an **superuser account**, use this command::

    $ docker-compose -f <local|production>.yml run --rm django python manage.py createsuperuser

For convenience, you can keep your normal user logged in on Chrome and your superuser logged in on Firefox (or similar), so that you can see how the site behaves for both kinds of users.

Manage data with fixtures
^^^^^^^^^^^^^^^^^^^^^^^^^

* You can load and dump data into the database models with the django manage.py script. This is an effective way to load test data, or static data, and also to backup data.

* To load JSON data from ``ramrod_rider_finder/riderstatus/fixtures/sagvehicle.json`` into *riderstatus.sagvehicle*, run the following command::

    $ docker-compose -f <local|production>.yml run --rm django python manage.py loaddata sagvehicle

* To dump the current database entries for the *sagvehicle* model within the *riderstatus* app, run the following command::

    $ docker-compose -f <local|production>.yml run --rm django python manage.py dumpdata riderstatus.sagvehicle | grep '^\[{' > sagvehicle_dump.json


Test coverage
^^^^^^^^^^^^^

To run the tests, check your test coverage, and generate an HTML coverage report::

    $ coverage run manage.py test
    $ coverage html
    $ open htmlcov/index.html

Running tests with py.test
~~~~~~~~~~~~~~~~~~~~~~~~~~

::

  $ py.test

Live reloading and Sass CSS compilation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Moved to `Live reloading and SASS compilation`_.

.. _`Live reloading and SASS compilation`: http://cookiecutter-django.readthedocs.io/en/latest/live-reloading-and-sass-compilation.html




Email Server
^^^^^^^^^^^^

In development, it is often nice to be able to see emails that are being sent from your application. For that reason local SMTP server `MailHog`_ with a web interface is available as docker container.

Container mailhog will start automatically when you will run all docker containers.
Please check `cookiecutter-django Docker documentation`_ for more details how to start all containers.

With MailHog running, to view messages that are sent by your application, open your browser and go to ``http://127.0.0.1:8025``

.. _mailhog: https://github.com/mailhog/MailHog



Sentry
^^^^^^

Sentry is an error logging aggregator service. You can sign up for a free account at  https://sentry.io/signup/?code=cookiecutter  or download and host it yourself.
The system is setup with reasonable defaults, including 404 logging and integration with the WSGI application.

You must set the DSN url in production.


Deployment
----------

The following details how to deploy this application.



Docker
^^^^^^

See detailed `cookiecutter-django Docker documentation`_.

.. _`cookiecutter-django Docker documentation`: http://cookiecutter-django.readthedocs.io/en/latest/deployment-with-docker.html

