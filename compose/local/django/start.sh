#!/bin/sh

set -o errexit
set -o pipefail
set -o nounset
set -o xtrace


# Collect static files
echo "Collect static files"
python manage.py collectstatic --noinput

# Create any new database migrations
echo "Create database migrations"
python manage.py makemigrations

# Apply database migrations
echo "Apply database migrations"
python manage.py migrate

python manage.py runserver_plus 0.0.0.0:8000
