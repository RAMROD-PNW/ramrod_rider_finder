from rest_framework_json_api import serializers

from ramrod_rider_finder.riderstatus.models import Rider, RiderStatus, RiderStatusBulk, Checkpoint


class RiderStatusSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = RiderStatus
        fields = '__all__'

    def validate(self, data):
        # Ensure foreign-key lookup fields provided map to actual objects
        if not Rider.objects.filter(bib_number=data['bib_number']).exists():
            raise serializers.ValidationError("Rider with bib number {0} does not exist".format(data['bib_number']))

        if not Checkpoint.objects.filter(code=data['cpcode']).exists():
            raise serializers.ValidationError("Checkpoint with code {0} does not exist".format(data['cpcode']))

        return data


class RiderStatusBulkSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = RiderStatusBulk
        fields = '__all__'

    def validate(self, data):
        # TODO: Validate CSV, but leave actual data verification to RiderStatusSerializer ?
        # Or nothing here?

        return data


class RiderSerializer(serializers.HyperlinkedModelSerializer):
    status = RiderStatusSerializer(
        many=True,
        read_only=True,
        allow_null=True
    )

    class Meta:
        model = Rider
        fields = '__all__'


class CheckpointSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Checkpoint
        fields = '__all__'
