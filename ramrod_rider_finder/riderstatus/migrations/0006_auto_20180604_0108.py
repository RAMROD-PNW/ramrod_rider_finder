# Generated by Django 2.0.3 on 2018-06-04 01:08

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('riderstatus', '0005_remove_rider_status'),
    ]

    operations = [
        migrations.AlterField(
            model_name='riderstatus',
            name='rider',
            field=models.ForeignKey(blank=True, editable=False, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='status', to='riderstatus.Rider'),
        ),
    ]
