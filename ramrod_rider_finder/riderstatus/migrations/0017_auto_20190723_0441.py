# Generated by Django 2.0.3 on 2019-07-23 04:41

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('riderstatus', '0016_auto_20190722_1535'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='rider',
            options={'ordering': ['-roster_year', 'bib_number', 'last_name', 'first_name']},
        ),
    ]
