# Generated by Django 2.0.3 on 2019-07-22 15:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('riderstatus', '0015_rider_roster_year'),
    ]

    operations = [
        migrations.AlterField(
            model_name='rider',
            name='bib_number',
            field=models.IntegerField(verbose_name='Bib Number'),
        ),
    ]
