# Generated by Django 2.0.3 on 2019-07-18 16:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('riderstatus', '0012_auto_20180722_2215'),
    ]

    operations = [
        migrations.CreateModel(
            name='RiderStatusBulk',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('csv_text', models.TextField(null=True, verbose_name='Raw CSV Data')),
                ('csv_received_time', models.DateTimeField(auto_now=True, verbose_name='Raw CSV Received Time')),
                ('csv_data_commited', models.BooleanField(default=False, verbose_name='RiderStatus Objects Created from CSV Document')),
            ],
            options={
                'verbose_name_plural': 'Rider Status Bulk CSV Submissions',
                'ordering': ['-csv_received_time'],
                'get_latest_by': ['-csv_received_time'],
            },
        ),
        migrations.AlterModelOptions(
            name='riderstatus',
            options={'get_latest_by': ['-crossing_time', '-data_received_time'], 'ordering': ['-data_received_time'], 'verbose_name_plural': 'Rider Statuses'},
        ),
    ]
