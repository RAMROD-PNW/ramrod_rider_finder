import datetime
import json

from django.test import TestCase
from rest_framework.test import APIClient

from ramrod_rider_finder.riderstatus.models import *
from ramrod_rider_finder.users.models import *


class RiderTest(TestCase):
    def setUp(self):
        Rider.objects.create(
            bib_number=9999,
            first_name="Test",
            last_name="Rider",
            gender="M",
            age=datetime.datetime.now().year - 1971,
            phone="+1 (555) 555-5555",
            emerg_contact="Emergency Contact",
            emerg_phone="+1 (555) 123-3232",
            tshirt_size="Large",
            breakfast=True,
            update_count=0,
            inquiry_count_public=0,
            inquiry_count_private=0
        )

    def test_full_name(self):
        rider = Rider.objects.get(bib_number=9999)
        self.assertEqual(rider.full_name(), 'Test Rider')
        self.assertEqual(str(rider), '9999: Test Rider')

    def test_initial_counters(self):
        rider = Rider.objects.get(bib_number=9999)
        self.assertEqual(rider.update_count, 0)
        self.assertEqual(rider.inquiry_count_public, 0)
        self.assertEqual(rider.inquiry_count_private, 0)
        self.assertEqual(rider.status_count(), 0)


class RiderStatusTest(TestCase):
    def setUp(self):
        # Create a rider, same as from RiderTest
        rider_test = RiderTest()
        rider_test.setUp()

        # Create some Checkpoints
        Checkpoint.objects.create(name="Start Line", code="STX")
        Checkpoint.objects.create(name="Finish Line", code="FIN")

        # Create a RiderStatus entry
        RiderStatus.objects.create(cpcode="STX", bib_number=9999, crossing_time="08:03", status="RFD")

    def test_update_count(self):
        rider = Rider.objects.get(bib_number=9999)
        self.assertEqual(rider.update_count, 1)

    def test_json_api_update_submit(self):
        rider_status_json = {
            "data": {
                "type": "RiderStatus",
                "attributes": {
                    "bib_number": 9999,
                    "cpcode": "FIN",
                    "status": "RFD",
                    "crossing_time": "14:10"
                }
            }
        }

        def send_post(auth_token=None):
            c = APIClient()

            if auth_token:
                c.credentials(HTTP_AUTHORIZATION='Token ' + auth_token)
            else:
                c.credentials()

            post_response = c.post(
                '/api/riderstatus/',
                json.dumps(rider_status_json),
                content_type='application/vnd.api+json',
                secure=True
            )

            return post_response

        def add_user_get_token():
            User.objects.create(username='testuser')
            return Token.objects.get(user__username='testuser').key

        # Send w/o authenticating - should fail with a 403
        unauth_response = send_post()
        self.assertEqual(unauth_response.status_code, 403)

        # Send with auth token from test user account
        token = add_user_get_token()
        auth_response = send_post(auth_token=token)
        self.assertEqual(auth_response.status_code, 201)

        # Check to see if the update POST has caused the rider to be updated
        rider = Rider.objects.get(bib_number=9999)
        self.assertEqual(rider.update_count, 2)
        self.assertEqual(str(rider.status.latest().crossing_time), '14:10:00')
