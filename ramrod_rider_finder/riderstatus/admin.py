from django.contrib import admin
from django.db import models

from .models import Rider, Checkpoint, SagVehicle, RiderStatus, RiderStatusBulk


class RiderStatusInline(admin.TabularInline):
    model = RiderStatus


@admin.register(Rider)
class RiderAdmin(admin.ModelAdmin):
    list_display = ('roster_year', 'bib_number', 'first_name', 'last_name', 'rfid5digit', 'status_count', 'update_count', 'inquiry_count_private', 'inquiry_count_public')
    search_fields = (
        'roster_year',
        'bib_number',
        'first_name',
        'last_name',
        'gender',
        'age',
        'phone',
        'emerg_contact',
        'emerg_phone',
        'tshirt_size',
        'breakfast',
        'rfid5digit',
    )

    inlines = [
        RiderStatusInline,
    ]

    readonly_fields = [
        'roster_year',
        'update_count',
        'inquiry_count_private',
        'inquiry_count_public',
    ]

    def get_queryset(self, request):
        # def queryset(self, request): # For Django <1.6
        qs = super(RiderAdmin, self).get_queryset(request)
        # qs = super(CustomerAdmin, self).queryset(request) # For Django <1.6
        qs = qs.annotate(models.Count('status'))
        return qs

    def status_count(self, obj):
        return obj.status__count

    status_count.admin_order_field = 'status__count'


@admin.register(Checkpoint)
class CheckpointAdmin(admin.ModelAdmin):
    list_display = ('code', 'name', 'distance')
    search_fields = ('code', 'name', 'info')


@admin.register(SagVehicle)
class SagVehicleAdmin(admin.ModelAdmin):
    list_display = ('sag_id',)
    search_fields = ('sag_id',)


@admin.register(RiderStatus)
class RiderStatusAdmin(admin.ModelAdmin):
    list_display = (
        'rider',
        'checkpoint',
        'status',
        'crossing_time',
        'roster_year',
        'data_received_time',
    )

    readonly_fields = ['rider']
    #
    # exclude = []


@admin.register(RiderStatusBulk)
class RiderStatusBulkAdmin(admin.ModelAdmin):
    list_display = (
        'csv_text',
        'csv_received_time',
        'csv_data_commited',
    )

    readonly_fields = ['csv_text', 'csv_received_time', 'csv_data_commited']
