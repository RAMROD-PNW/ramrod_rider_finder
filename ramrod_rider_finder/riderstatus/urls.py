from django.conf.urls import url, include
from rest_framework import routers
from ramrod_rider_finder.riderstatus import views

router = routers.DefaultRouter(trailing_slash=True)
router.register(r'riders', views.RiderViewSet)
router.register(r'checkpoints', views.CheckpointViewSet)
router.register(r'riderstatus', views.RiderStatusUpdate)
router.register(r'riderstatusbulk', views.RiderStatusBulkUpdate)

urlpatterns = [
    url(r'^', include(router.urls)),
]
