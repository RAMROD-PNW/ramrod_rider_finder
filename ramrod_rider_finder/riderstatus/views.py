from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator

from rest_framework import viewsets
from rest_framework.permissions import AllowAny, IsAuthenticated

from ramrod_rider_finder.riderstatus.models import Rider, RiderStatus, RiderStatusBulk, Checkpoint
from ramrod_rider_finder.riderstatus.serializers import RiderSerializer, RiderStatusSerializer, RiderStatusBulkSerializer, CheckpointSerializer


class RiderViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows riders to be viewed or edited.
    """
    permission_classes = (IsAuthenticated,)

    queryset = Rider.objects.all()
    serializer_class = RiderSerializer


@method_decorator(csrf_exempt, name='dispatch')  # FIXME: https://gitlab.com/RAMROD-PNW/ramrod_rider_finder/issues/8
class RiderStatusUpdate(viewsets.ModelViewSet):
    """
    API endpoint that accepts updates from the field for rider status updates
    """
    permission_classes = (AllowAny,)

    queryset = RiderStatus.objects.all()
    serializer_class = RiderStatusSerializer


@method_decorator(csrf_exempt, name='dispatch')  # FIXME: https://gitlab.com/RAMROD-PNW/ramrod_rider_finder/issues/8
class RiderStatusBulkUpdate(viewsets.ModelViewSet):
    """
    API endpoint that accepts bulk CSV updates from the field for rider status updates
    """
    permission_classes = (AllowAny,)

    queryset = RiderStatusBulk.objects.all()
    serializer_class = RiderStatusBulkSerializer


class CheckpointViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint for Checkpoints
    """
    permission_classes = (IsAuthenticated,)

    queryset = Checkpoint.objects.all()
    serializer_class = CheckpointSerializer
