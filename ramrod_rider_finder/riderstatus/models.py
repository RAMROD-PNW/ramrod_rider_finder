import csv
import datetime
import io
from django.db import models
# from django.urls import reverse
from django.utils.translation import ugettext_lazy as _


class Rider(models.Model):
    #       bib		SMALLINT	NOT NULL	PRIMARY KEY
    # ,	    last_name	VARCHAR(16)	NOT NULL
    # ,	    first_name	VARCHAR(16)	NOT NULL
    # ,	    gender		VARCHAR(8)	NOT NULL
    # ,	    age			TINYINT		NOT NULL

    # ,	    rfid5digit	SMALLINT	NOT NULL

    # ,	    stat		CHAR(3)		NULL
    # #,	phone		VARCHAR(12)	NULL

    # #,	emerg_contact	VARCHAR(25)	NULL
    # #,	emerg_phone	VARCHAR(12)	NULL

    # #,	tshirt_size	VARCHAR(15)	NULL
    # #,	breakfast	TINYINT		NULL

    # ,	    startx	TIME		NULL	DEFAULT NULL
    # #,	buckly	TIME		NULL	DEFAULT NULL
    # ,	    nisqul	TIME		NULL	DEFAULT NULL
    # ,	    backbn	TIME		NULL	DEFAULT NULL
    # ,	    cayuse	TIME		NULL	DEFAULT NULL
    # #,	greenwater	time	NULL	DEFAULT NULL
    # ,	    finish	TIME		NULL	DEFAULT NULL

    # Metadata about this instance
    # Store the year this Rider instance was created, e.g. what year event is this data for
    roster_year = models.PositiveSmallIntegerField(_('Roster Year'), blank=False, null=False,
                                                   default=datetime.datetime.now().year)
    # Basic Rider Details
    # We need the bib number of a user because it's how we look them up elsewhere
    bib_number = models.IntegerField(_('Bib Number'), blank=False, null=False, unique=False)
    rfid5digit = models.IntegerField(_('RFID Tag Value'), blank=True, default=-1, unique=False)

    first_name = models.CharField(_('First Name'), blank=False, default='', max_length=256)
    last_name = models.CharField(_('Last Name'), blank=False, default='', max_length=256)

    gender = models.CharField(_('Gender'), blank=True, default='', max_length=32)
    age = models.SmallIntegerField(_('Age'), blank=True, default=-1)

    phone = models.CharField(_('Phone Number'), blank=True, default='', max_length=32)

    emerg_contact = models.CharField(_('Emergency Contact'), blank=True, default='', max_length=256)
    emerg_phone = models.CharField(_('Emergency Contact Phone NUmber'), blank=True, default='', max_length=32)

    tshirt_size = models.CharField(_('T-Shirt Size'), blank=True, default='', max_length=32)
    breakfast = models.NullBooleanField(_('Marked for Breakfast'), blank=True, default=None)

    # Store how many times we have received data updates for this rider
    update_count = models.IntegerField(blank=False, default=0, editable=False)

    # Store how many times anyone from the public has looked up this rider
    inquiry_count_public = models.IntegerField(blank=False, default=0, editable=False)

    # Store how many times any admin/volunteer users have looked up this rider
    inquiry_count_private = models.IntegerField(blank=False, default=0, editable=False)

    def full_name(self):
        return "{0} {1}".format(self.first_name, self.last_name)

    def status_count(self):
        return self.status.count()

    def __str__(self):
        return "{bib_number}: {full_name}".format(bib_number=self.bib_number, full_name=self.full_name())

    class Meta:
        ordering = ['-roster_year', 'bib_number', 'last_name', 'first_name']


class Checkpoint(models.Model):
    """
    A model representing an individual checkpoint location
    """
    name = models.CharField(_('Name'), blank=False, max_length=256)
    code = models.CharField(_('Code'), blank=False, max_length=3, unique=True)
    latitude = models.DecimalField(_('Latitude'), max_digits=9, decimal_places=6, blank=True, null=True)
    longitude = models.DecimalField(_('Longitude'), max_digits=9, decimal_places=6, blank=True, null=True)
    distance = models.DecimalField(_('Distance'), max_digits=5, decimal_places=2, blank=True, null=True)
    info = models.TextField(_('Information'), blank=True, max_length=2 ** 16)

    def __str__(self):
        # return "{code}: {name}".format(code=self.code, name=self.name)
        return "{name}".format(name=self.name)

    class Meta:
        ordering = ['distance']


class SagVehicle(models.Model):
    sag_id = models.SmallIntegerField(_('Vehicle ID'), blank=False, primary_key=True)

    class Meta:
        ordering = ['sag_id']


class RiderStatus(models.Model):
    # These must be set on save and excluded from the API serializer
    # Store the year this Rider instance was created, e.g. what year event is this data for
    roster_year = models.PositiveSmallIntegerField(_('Roster Year'), blank=False, null=False,
                                                   default=datetime.datetime.now().year)

    rider = models.ForeignKey(Rider, related_name='status', editable=False, null=True, blank=True, on_delete=models.PROTECT)
    checkpoint = models.ForeignKey(Checkpoint, related_name='checkpoint', on_delete=models.PROTECT, editable=False, default=0)

    bib_number = models.IntegerField(_('Bib Number'), blank=False, null=True)
    cpcode = models.CharField(_('Checkpoint Code'), blank=False, null=True, max_length=32)

    # TODO: Refactor and make this RiderData class, and use RiderStatus class and models for each status type?
    status = models.CharField(_('Rider Status'), blank=False, max_length=32)

    # crossing_time is the time received via the API for this update message
    crossing_time = models.TimeField(_('Crossing Time'), blank=False)

    # data_received_time is the time at which we received this status message and created the record
    data_received_time = models.DateTimeField(_('Data Received Time'), auto_now=True, blank=False, editable=False)

    def render_status(self):
        if self.status.upper() == 'RFD':
            status_msg = _('Through Checkpoint')
        elif self.status.upper() == 'MAN':
            status_msg = _('Through Checkpoint')
        elif self.status.upper().startswith('SG'):
            sag_id = self.status.upper().rsplit('SG', 1)[-1]
            if sag_id.strip().strip('?') == '':
                sag_id = 'Unknown'
            status_msg = _("SAG Transport - SAG Vehicle ID: {sag_id}".format(sag_id=sag_id))
        elif self.status.upper() == 'QUT':
            status_msg = _('Rider Disqualified or Quit For Other Reason - Does Not Require SAG Vehicle')
        else:
            status_msg = _('Error/Unknown Status')

        return status_msg

    def __str__(self):
        return "{time}: {rider} at Checkpoint {checkpoint}".format(time=self.crossing_time, rider=self.rider.full_name(), checkpoint=self.checkpoint.name)

    def save(self, *args, **kwargs):
        # Set foreign key relations based on model fields (parameters are safely validated in RiderStatusSerializer.validate)
        self.rider = Rider.objects.get(bib_number=self.bib_number, roster_year=self.roster_year)
        self.checkpoint = Checkpoint.objects.get(code=self.cpcode)

        # Fix up crossing_time entry without timezone info. Force check because the test framework seems to cast it as a string.
        if isinstance(self.crossing_time, models.TimeField) and self.crossing_time.tzinfo:
            self.crossing_time = self.crossing_time.replace(tzinfo=None)

        # Increment the update_count field on the rider we're saving a record for
        self.rider.update_count += 1
        self.rider.save()

        super().save(*args, **kwargs)  # Call the "real" save() method.

    class Meta:
        verbose_name_plural = 'Rider Statuses'
        ordering = ['-data_received_time']
        get_latest_by = ['-crossing_time', '-data_received_time']


class RiderStatusBulk(models.Model):
    # Store raw CSV data in the database before processing it to RiderStatus objects in case of any
    # failure situations.
    csv_text = models.TextField(_('Raw CSV Data'), blank=False, null=True)

    # data_received_time is the time at which we received this status message and created the record
    csv_received_time = models.DateTimeField(_('Raw CSV Received Time'), auto_now=True, blank=False, editable=False)

    # Flag if we have already commited the data in this document (e.g. created RiderStatus objects
    # out of it all)
    csv_data_commited = models.BooleanField(_('RiderStatus Objects Created from CSV Document'), default=False, null=False, editable=False)

    def save(self, *args, **kwargs):
        fieldnames = ['bib_number', 'cpcode', 'status', 'crossing_time']
        riderstatus_dicts = RiderStatusBulk.csv_to_dicts(self.csv_text, fieldnames)

        # Only run the following portion of the save code if we have NOT done so before (avoid
        # creating duplicate RiderStatus entries)
        if not self.csv_data_commited:
            RiderStatusBulk.commit_riderstatus_dicts(riderstatus_dicts)

            self.csv_data_commited = True

            super().save(*args, **kwargs)  # Call the "real" save() method.

    @staticmethod
    def csv_to_dicts(csv_text, fieldnames):
        csv_file = io.StringIO(csv_text)
        reader = csv.DictReader(csv_file, fieldnames=fieldnames)

        riderstatus_dicts = []

        for row in reader:
            riderstatus_dicts.append(row)

        return riderstatus_dicts

    @staticmethod
    def commit_riderstatus_dicts(riderstatus_dicts):
        for rs in riderstatus_dicts:
            try:
                rider_status = RiderStatus()
                rider_status.bib_number = rs['bib_number']
                rider_status.cpcode = rs['cpcode']
                rider_status.status = rs['status']
                rider_status.crossing_time = rs['crossing_time']
                rider_status.save()
            except Exception as ex:
                raise

    class Meta:
        verbose_name_plural = 'Rider Status Bulk CSV Submissions'
        ordering = ['-csv_received_time']
        get_latest_by = ['-csv_received_time']


# class PhoneNumber(models.Model):
#     number = models.CharField(_('Phone Number'), blank=False, null=False, max_length=32)
#     messages_sent = models.IntegerField(_('Messages Sent'), default=0, blank=False, null=False)
#
#
# class Notification(models.Model):
#     phone_number = models.ForeignKey(PhoneNumber, editable=False, null=False, blank=False, on_delete=models.PROTECT)
#     rider = models.ForeignKey(Rider, editable=False, null=False, blank=False, on_delete=models.PROTECT)
#
#     class Meta:
#         order_with_respect_to = 'Rider'
