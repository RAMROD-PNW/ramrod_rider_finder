from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.views.generic import TemplateView
from django.views import defaults as default_views
from django.conf.urls import url, include

from ramrod_rider_finder.riderstatusweb.views import *

urlpatterns = [
    url(r'^rider/lookup/$', TemplateView.as_view(template_name='pages/rider_lookup_form.html'), name='rider_lookup_form'),
    url(r'^rider/lookup/results/$', RiderStatusView.as_view(), name='rider_lookup_results'),
    url(r'^rider/list/$', AuthenticatedRiderListView.as_view(), name='auth_rider_list_view'),
    url(r'^riderstatus/list/$', AuthenticatedRiderStatusListView.as_view(), name='auth_rider_status_list_view'),
]
