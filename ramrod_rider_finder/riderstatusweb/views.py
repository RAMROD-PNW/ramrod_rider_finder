import datetime
from django.db.models import Q
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.views.generic.base import TemplateView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView

from ramrod_rider_finder.riderstatus.models import Rider, Checkpoint, RiderStatus


class RiderStatusView(TemplateView):
    MIN_NAME_QUERY_LEN = 3
    CURRENT_YEAR = datetime.datetime.now().year

    template_name = 'pages/rider_lookup_results.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        rider = None

        # URL Parameters
        try:
            bib_number_param = int(self.request.GET.get('bib', -1))
        except ValueError:
            context['error'] = 'Invalid rider bib number provided, please try again'
            return context

        try:
            roster_year_param = int(self.request.GET.get('year', -1))
        except ValueError:
            context['error'] = 'Invalid roster year provided, please try again'
            return context

        last_name_param = self.request.GET.get('last_name', '')

        # Find single rider object based on last name and bib number
        results = list(Rider.objects.filter(Q(bib_number=bib_number_param)
                                            & Q(last_name__istartswith=last_name_param)
                                            & Q(roster_year=roster_year_param)))

        if len(results) == 1:
            rider = results[0]
        elif len(results) < 1:
            context['error'] = 'No rider found, please try with different parameters'
        elif len(results) > 1:
            context['error'] = 'Multiple results found, please provide a more specific last name'

        # In general, we only return a result if the bib number is an exact match, and if the first 3 letters of the last name match.
        # However, if a person has a last name of less than 3 characters, we have some logic to allow it if it's an exact match.
        if len(last_name_param) < RiderStatusView.MIN_NAME_QUERY_LEN:
            if rider:
                if len(rider.last_name) >= RiderStatusView.MIN_NAME_QUERY_LEN:
                    context['error'] = "At least 3 characters of the rider's last name must be provided"
                elif len(rider.last_name) > len(last_name_param):
                    context['error'] = 'Please provide a more specific last name'

        if not rider:
            return context

        # At this point we can trust we have a good Rider object and no errors
        # Let's get their status entries
        rider_status_entries = list(rider.status.all().order_by('-crossing_time'))
        if rider_status_entries:
            latest_rider_status = rider_status_entries[0]
            latest_rider_distance = latest_rider_status.checkpoint.distance
        else:
            latest_rider_status = None
            latest_rider_distance = 0

        # Get the last checkpoint's mileage to use as our 100% point for percent complete, etc.
        finish_mileage = Checkpoint.objects.order_by('-distance').first().distance

        # If there's no error message, add the Rider and RiderStatus objects to the context
        if 'error' not in context:
            context['rider'] = rider
            context['rider_status_entries'] = rider_status_entries
            context['latest_rider_status'] = latest_rider_status
            context['finish_mileage'] = finish_mileage
            context['progress_percentage'] = 100 * latest_rider_distance / finish_mileage

            # Finally, right before we return successfully, if no errors and we have a rider, increment this rider's inquiry counter
            if rider:
                if self.request.user.is_authenticated:
                    # Authenticated - means the person viewing is admin/staff/volunteer
                    rider.inquiry_count_private += 1
                else:
                    # Unauthenticated - means the person viewing is someone in the general public
                    rider.inquiry_count_public += 1

                # Save the updated Rider object
                rider.save()

        return context


class AuthenticatedRiderListView(LoginRequiredMixin, ListView):
    template_name = 'pages/rider_list.html'
    model = Rider
    paginate_by = 100

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        return context


class AuthenticatedRiderStatusListView(LoginRequiredMixin, ListView):
    template_name = 'pages/rider_status_list.html'
    model = RiderStatus
    paginate_by = 100

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        return context
